<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/orders/total',function(Request $request){
    $currentTotal = DB::table('dummyorders')->first();
    $yesOrNot = rand(0,1);

    if($yesOrNot%2==0){
      $newOrderAmount = mt_rand (1*100, 100*100) / 100;
      $currentTotal->total += $newOrderAmount;
      DB::table('dummyorders')->where('id',$currentTotal->id)->update(['total' => $currentTotal->total]);
    }

    $return = [
        'data' => [
          'total' =>   number_format($currentTotal->total,2,',','.')
        ]
    ];
    return $return;
});
