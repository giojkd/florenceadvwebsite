<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


  <title>Florence ADV - No chatters, facts!</title>
</head>
<body>

  <div class="vh-100 vw-100 d-flex justify-content-center">
    <div class="vh-25 vw-25 align-self-center bg-light p-4">
      <h1 class="d-none">Florence ADV</h1>
      <img height="80" src="/florence-adv-logo.png" alt="">
      <h2>Web Agency a Firenze</h2>
      <p>"We Do eCommerce" da sempre.</p>
      <p>"We Love Perfection" come nessun altro.</p>
      <p>"We Rush" verso i risultati.</p>
      <p>"We DO NOT overload" curiamo ogni progetto come se fosse l'unico.</p>
      <hr>
      <p>"No chatters, facts!" andiamo dritti al punto.</p>
      <h3>EUR <span>2.832.345.234,60</span></h3>
      Venduti sui nostri ecommerce da quando abbiamo iniziato. (and going)
      <hr>
      <address>
        <strong>Florence ADV srls</strong>
        <p>
          <small class="text-muted">
          Via Basili 4/C, Santa Croce sull'Arno 56029 (Fi)<br>
          P.iva 02286430505<br>
          Email: <a href="mailto:info@florenceadv.it">info@florenceadv.it</a><br>
          Phone: <a href="tel:+393289725887">+393289725887</a>
          </small>
        </p>
      </address>
    </div>
  </div>
  <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css?family=Roboto+Mono:300,400&display=swap" rel="stylesheet">
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
